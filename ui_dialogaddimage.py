# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialogaddimage.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_DialogAddImage(object):
    def setupUi(self, DialogAddImage):
        DialogAddImage.setObjectName("DialogAddImage")
        DialogAddImage.resize(467, 97)
        DialogAddImage.setModal(True)
        self.buttonBox = QtWidgets.QDialogButtonBox(DialogAddImage)
        self.buttonBox.setGeometry(QtCore.QRect(80, 60, 371, 23))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.lineEditImagePath = QtWidgets.QLineEdit(DialogAddImage)
        self.lineEditImagePath.setGeometry(QtCore.QRect(100, 20, 261, 20))
        self.lineEditImagePath.setObjectName("lineEditImagePath")
        self.pushButtonBrowse = QtWidgets.QPushButton(DialogAddImage)
        self.pushButtonBrowse.setGeometry(QtCore.QRect(370, 20, 81, 23))
        self.pushButtonBrowse.setObjectName("pushButtonBrowse")
        self.label = QtWidgets.QLabel(DialogAddImage)
        self.label.setGeometry(QtCore.QRect(10, 20, 81, 21))
        self.label.setObjectName("label")

        self.retranslateUi(DialogAddImage)
        self.buttonBox.accepted.connect(DialogAddImage.accept)
        self.buttonBox.rejected.connect(DialogAddImage.reject)
        QtCore.QMetaObject.connectSlotsByName(DialogAddImage)
        DialogAddImage.setTabOrder(self.lineEditImagePath, self.pushButtonBrowse)
        DialogAddImage.setTabOrder(self.pushButtonBrowse, self.buttonBox)

    def retranslateUi(self, DialogAddImage):
        _translate = QtCore.QCoreApplication.translate
        DialogAddImage.setWindowTitle(_translate("DialogAddImage", "Add raster for interactive georeferencing"))
        self.pushButtonBrowse.setText(_translate("DialogAddImage", "Browse..."))
        self.label.setText(_translate("DialogAddImage", "Image path"))

