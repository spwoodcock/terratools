# TerraTools

QGIS plugin dev project 2018-2019.

Developed after project scoping of requirements amongst employees of mining client.

Tools to manipulate raster imagery without re-registration, extract geometry coordinates, and ease digitisation workflows.
