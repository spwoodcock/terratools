# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialogexportgeorast.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_DialogExportGeoRast(object):
    def setupUi(self, DialogExportGeoRast):
        DialogExportGeoRast.setObjectName("DialogExportGeoRast")
        DialogExportGeoRast.resize(458, 94)
        self.buttonBox = QtWidgets.QDialogButtonBox(DialogExportGeoRast)
        self.buttonBox.setGeometry(QtCore.QRect(110, 60, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.lineEditImagePath = QtWidgets.QLineEdit(DialogExportGeoRast)
        self.lineEditImagePath.setGeometry(QtCore.QRect(100, 10, 261, 20))
        self.lineEditImagePath.setObjectName("lineEditImagePath")
        self.pushButtonBrowse = QtWidgets.QPushButton(DialogExportGeoRast)
        self.pushButtonBrowse.setGeometry(QtCore.QRect(370, 10, 81, 23))
        self.pushButtonBrowse.setObjectName("pushButtonBrowse")
        self.label = QtWidgets.QLabel(DialogExportGeoRast)
        self.label.setGeometry(QtCore.QRect(10, 10, 81, 21))
        self.label.setObjectName("label")
        self.checkBoxRotationMode = QtWidgets.QCheckBox(DialogExportGeoRast)
        self.checkBoxRotationMode.setGeometry(QtCore.QRect(10, 40, 161, 17))
        self.checkBoxRotationMode.setObjectName("checkBoxRotationMode")

        self.retranslateUi(DialogExportGeoRast)
        self.buttonBox.accepted.connect(DialogExportGeoRast.accept)
        self.buttonBox.rejected.connect(DialogExportGeoRast.reject)
        QtCore.QMetaObject.connectSlotsByName(DialogExportGeoRast)

    def retranslateUi(self, DialogExportGeoRast):
        _translate = QtCore.QCoreApplication.translate
        DialogExportGeoRast.setWindowTitle(_translate("DialogExportGeoRast", "Export georeferenced raster"))
        self.pushButtonBrowse.setText(_translate("DialogExportGeoRast", "Browse..."))
        self.label.setText(_translate("DialogExportGeoRast", "Image path"))
        self.checkBoxRotationMode.setToolTip(_translate("DialogExportGeoRast", "<html><head/><body><p>If checked, the raster will be exported as is and the world file will contain the rotation, scaling and translation. Some GIS software may not be able to georeference the raster correctly in this mode. If unchecked, the raster will be modified with the rotation and the scaling between axis and the world file will contain the rest of the transformation.</p></body></html>"))
        self.checkBoxRotationMode.setText(_translate("DialogExportGeoRast", "Put rotation in world file "))

