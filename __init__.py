# -*- coding: utf-8 -*-
"""
/***************************************************************************
                                    TerraTools
                                 A QGIS plugin
                    For digitisation & georeferencing of rasters
                             -------------------
        copyright            : (C) 2019 by Sam Woodcock
        email                : perth@terrasearch.com.au
 ***************************************************************************/
"""

# get .whl wheels from pypi.org
# math & numpy should be available by default
# csv not required if pandas installed
# numpy if required: numpy-1.18.1-cp35-cp35m-win_amd64.whl
# pandas: pandas-1.0.1-cp36-cp36m-win_amd64.whl
# try:
#     import pandas
# except ImportError:
#     import sys, os
#     this_dir = os.path.dirname(os.path.realpath(__file__))
#     path = os.path.join(this_dir, 'pandas-1.0.1-cp36-cp36m-win_amd64.whl')
#     sys.path.append(path)
#     import pandas


def classFactory(iface):
    from .terratools import TerraTools
    return TerraTools(iface)
